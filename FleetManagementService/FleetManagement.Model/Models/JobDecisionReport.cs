﻿using System;
using FleetManagement.Core.Enumerations;

namespace FleetManagement.Core.Models
{
    public class JobDecisionReport
    {
        public JobDecisionReport()
        {
            JobDecision = JobDecision.Unknown;
            Description = string.Empty;
        }

        public JobDecision JobDecision { get; set; }
        public string Description { get; set; }
    }
}
