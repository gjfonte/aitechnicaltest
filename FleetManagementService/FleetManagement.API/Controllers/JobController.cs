﻿using FleetManagement.Core.Models;
using FleetManagement.Service;
using Microsoft.AspNetCore.Mvc;

namespace FleetManagement.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class JobController : ControllerBase
    {
        private readonly IJobSheetService _jobSheetService;

        public JobController(IJobSheetService jobSheetService)
        {
            _jobSheetService = jobSheetService;
        }

        // GET api/job/generate
        [HttpGet("generate")]
        public ActionResult<JobSheet> GenerateJobSheet()
        {
            return Ok(_jobSheetService.Generate());
        }

        // POST api/job/submit
        [HttpPost("submit")]
        public ActionResult<JobDecisionReport> SubmitJobSheet([FromBody] JobSheet jobSheet)
        {
            return Ok(_jobSheetService.ValidateJobSheet(jobSheet));
        }
    }
}
