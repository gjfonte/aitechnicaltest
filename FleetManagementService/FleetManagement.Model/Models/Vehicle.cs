﻿namespace FleetManagement.Core.Models
{
    public class Vehicle
    {
        public int VehicleID { get; set; }
        public int Year { get; set;  }
        public string Make { get; set; }
        public string Model { get; set; }
        public string RegistrationPlate { get; set; }
    }
}