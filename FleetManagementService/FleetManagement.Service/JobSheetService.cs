﻿using System;
using System.Collections.Generic;
using System.Linq;
using FleetManagement.Core;
using FleetManagement.Core.Enumerations;
using FleetManagement.Core.Models;
using FleetManagement.Data;

namespace FleetManagement.Service
{
    /// <summary>
    /// Class responsible to manage job sheets
    /// </summary>
    public class JobSheetService : IJobSheetService
    {
        private readonly MockData _mockData;

        public JobSheetService()
        {
            _mockData = new MockData();
        }

        /// <summary>
        /// Generate a random job sheet
        /// </summary>
        /// <returns>The a job sheet.</returns>
        public JobSheet Generate()
        {
            return GenerateRandomJobSheet();
        }

        /// <summary>
        /// Validate JobSheet
        /// </summary>
        /// <returns>The a report with the decision.</returns>
        /// <param name="jobSheet">Job sheet.</param>
		public JobDecisionReport ValidateJobSheet(JobSheet jobSheet)
        {
            // 1: Business Rules:

            // Tyres must be changed in pairs, and a job sheet can include a maximum of 4.
            var totalTyres = jobSheet.WorkItems.Where(w => w.Item == WorkItemType.Tyre).ToList().Count;
            if (totalTyres > 4)
            {
                return new JobDecisionReport()
                {
                    JobDecision = JobDecision.Declined,
                    Description = Messages.TyresCannotExceed4
                };
            }

            // Tyres must be changed in pairs, and a job sheet can include a maximum of 4.
            if (totalTyres % 2 != 0)
            {
                return new JobDecisionReport()
                {
                    JobDecision = JobDecision.Declined,
                    Description = Messages.TyresMustBeChangedInPairs
                };
            }

            // Brake pads and discs must be changed at the same time
            var totalBrackePads = jobSheet.WorkItems.Where(w => w.Item == WorkItemType.BrakePad).ToList().Count;
            var totalBrackeDiscs = jobSheet.WorkItems.Where(w => w.Item == WorkItemType.BrakeDisc).ToList().Count;
            if (totalBrackePads != totalBrackeDiscs)
            {
                return new JobDecisionReport()
                {
                    JobDecision = JobDecision.Declined,
                    Description = Messages.BrakePadsAndDiscsMustBeChangedAtSameTime
                };
            }

            // A job sheet can only a include a maximum of 1 exhaust.
            var totalExausts = jobSheet.WorkItems.Where(w => w.Item == WorkItemType.Exhaust).ToList().Count;
            if (totalExausts > 1)
            {
                return new JobDecisionReport()
                {
                    JobDecision = JobDecision.Declined,
                    Description = Messages.CanOnlyInclude1Exhaust
                };
            }

            // The total hours labour must not exceed the reference number of hours labour.
            var isValid = jobSheet.IsLabourTimeValid();
            if (isValid == false)
            {
                return new JobDecisionReport()
                {
                    JobDecision = JobDecision.Declined,
                    Description = Messages.TotalHoursLabourMustNotExceedTheReferenceNumberOfHoursLabour
                };
            }

            // 2: Overall Decision
            return GetCostDecisionReport(jobSheet);
        }
        
        /// <summary>
        /// Generates a valid random job sheet.
        /// </summary>
        /// <returns>The random job sheet.</returns>
        private JobSheet GenerateRandomJobSheet()
        {
            var jobSheet = new JobSheet();
            var isValid = false;

            // generating with a while until we have got a valid job sheet.
            // if is not valid, it will generate a new job sheet
            while (isValid == false)
            {
                // create a new job sheet
                jobSheet = new JobSheet();
                jobSheet.JobDate = DateTime.Now.AddDays(-GetRandomNumber(7));

                // set vehicle
                var idxVehicle = GetRandomNumber(_mockData.Vehicles.Count - 1);
                jobSheet.Vehicle = _mockData.Vehicles[idxVehicle];

                // add tyres. Rule: job sheet can include a maximum of 4 tyres
                var totalTyresToInsert = GetRandomNumber(4);
                if (totalTyresToInsert == 1)
                {
                    // this is to help creating more acceptable job sheets
                    totalTyresToInsert = 2;
                }
                AddWorkItems(jobSheet, WorkItemType.Tyre, totalTyresToInsert);

                // add brake disc. Rule: brake pads and discs must be changed at the same time
                var totalBrakesToInsert = GetRandomNumber(2);
                AddWorkItems(jobSheet, WorkItemType.BrakePad, totalBrakesToInsert);
                AddWorkItems(jobSheet, WorkItemType.BrakeDisc, totalBrakesToInsert);

                // add oil. Rule: no rule
                var totalOilToInsert = GetRandomNumber(1);
                AddWorkItems(jobSheet, WorkItemType.Oil, totalOilToInsert);

                // add exhaust. Rule: A job sheet can only a include a maximum of 1 exhaust.
                var totalExhaustToInsert = GetRandomNumber(1);
                AddWorkItems(jobSheet, WorkItemType.Exhaust, totalExhaustToInsert);

                // calc total cost an validate job sheet
                jobSheet.TotalCost = jobSheet.GetLabourTotalCost();
                isValid = jobSheet.IsLabourTimeValid();
            }

            return jobSheet;
        }

        private void AddWorkItems(JobSheet jobSheet, WorkItemType workItemType, int total)
        {
            var workItems = _mockData.GetWorkItemsByType(workItemType);
            for (int i = 0; i < total; i++)
            {
                var idx = GetRandomNumber(workItems.Count - 1);
                jobSheet.WorkItems.Add(workItems[idx]);
            }
        }

        /// <summary>
        /// Gets the cost decision report.
        /// Approve - If the total price is within 10% of the reference price.
        /// Refer - If the total price is between 10% and 15% of the reference price.
        /// Decline - If the total price exceeds 15% of the reference price.
        /// </summary>
        /// <returns>The cost report.</returns>
        /// <param name="jobSheet">Job sheet.</param>
        private JobDecisionReport GetCostDecisionReport(JobSheet jobSheet)
        {
            var mockData = new MockData();

            // get total cost
            decimal _cost = 0;
            decimal _costReference = 0;

            foreach (var item in jobSheet.WorkItems)
            {
                // get job sheet cost
                decimal labourCost = item.TimeInMinutes * mockData.LabourCostPerHour / 60;
                _cost += item.Cost + labourCost;

                // get job sheet reference cost
                var referenceData = mockData.ReferenceData.FirstOrDefault(r => r.Item == item.Item);
                decimal referenceLabourCost = referenceData.TimeInMinutes * mockData.LabourCostPerHour / 60;
                _costReference += referenceData.Cost + referenceLabourCost;
            }

            // return decision
            decimal maxCost = 0;

            // Approve - If the total price is within 10% of the reference price.
            maxCost = _costReference * (decimal)1.1;
            if (_cost <= maxCost)
                return new JobDecisionReport()
                {
                    JobDecision = JobDecision.Approved,
                    Description = Messages.PriceIsWithin10PercentOfTheReferencePrice
                };

            // Refer - If the total price is between 10% and 15% of the reference price.
            maxCost = _costReference * (decimal)1.15;
            if (_cost <= maxCost)
                return new JobDecisionReport()
                {
                    JobDecision = JobDecision.Refered,
                    Description = Messages.TotalPriceIsBetween10and15PercentOfTheReferencePrice
                };

            // Decline - If the total price exceeds 15% of the reference price.
            return new JobDecisionReport()
            {
                JobDecision = JobDecision.Declined,
                Description = Messages.PriceExceeds15PercentOfTheReferencePrice
            };
        }

        private int GetRandomNumber(int max)
        {
            return new Random().Next(0, max + 1);
        }
    }
}
