﻿using System;
using FleetManagement.Core.Models;

namespace FleetManagement.Service
{
    public interface IJobSheetService
    {
        JobSheet Generate();

        JobDecisionReport ValidateJobSheet(JobSheet jobSheet);
    }
}
