# Fleet Management System

This is the full system responsible to generate, submit and valid job sheets.

## Getting Started

Firstly, we need to start the API service running on http://localhost:5001. Then start the Web-Based app which will be running on http://localhost:5002.

### Prerequisites

You need to install the following software:

```
.Net Core SDK 2.1
```

## Starting the whole system

You need to start the service API and Web UI application.


### Service API

Go to folder FleetManagement.API and execute the following command to start the service:

```
dotnet run
```

Then check if the API is running opening the following URL on your browser:

```
http://localhost:5001/api/job/generate
```
You should see a JSON file with a structured Job Sheet.


### Web UI

Go to folder FleetManagement.Web and execute the following command to start the service:

```
dotnet run
```

Then check if the Web page is running opening the following URL on your browser:

```
http://localhost:5002/
```
You should see a page with a **Generate button**.


# How to use the system

1. Create a new Job Sheet by clicking on the **Generate Job** button.
2. Review the Job and click **Submit** to validate the Job sheet.
3. Wait for the system to validate the job sheet and a page will open with the results of the validation.


## Menus

* Home - Go back to the main screen to generate a new job
* All jobs - View previous submitted jobs
* About - Details about me :)


## Unit Test

This project is covered by the following tests:

* GenerateReturnsJobSheet
* GenerateReturnsOk
* SubmitReturnsApproved
* SubmitReturnsBrakePadsAndDisksMustBeChangedAtSameTime
* SubmitReturnsCanOnlyIncludeOneExhaust
* SubmitReturnsDeclined
* SubmitReturnsJobDecision
* SubmitReturnsOk
* SubmitReturnsRefered
* SubmitReturnsTotalHoursMustNotExceedTheReferenceNumberOfHours
* SubmitReturnsTyresCannotExceedFour
* SubmitReturnsTyresMustBeChangedInPairs

To run the tests go to folder FleetManagement.Tests and execute the following command:

```
dotnet test
```

# Contributing

Please help us here [SourceTree](https://bitbucket.org/gjfonte/aitechnicaltest).


## Authors

* **Gilberto Fonte** - *Initial work* - [gjfonte](https://github.com/)

See also the list of [contributors](https://bitbucket.org/gjfonte/aitechnicaltest) who participated in this project.

