﻿using System;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;

namespace FleetManagement.Web.Controllers
{
    public class BaseController : Controller
    {
        protected IMemoryCache _cache;

        public BaseController(IMemoryCache cache)
        {
            _cache = cache;
        }

        public T Get<T>(string key)
        {
            if (_cache.TryGetValue(key, out T cacheEntry))
            {
                return cacheEntry;
            }

            return default(T);
        }

        public void Set<T>(string key, T cacheEntry)
        {
            _cache.Set(key, cacheEntry);
        }
    }
}
