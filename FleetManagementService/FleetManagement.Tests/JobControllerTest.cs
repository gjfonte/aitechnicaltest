using System.Collections.Generic;
using System.Linq;
using FleetManagement.API.Controllers;
using FleetManagement.Core;
using FleetManagement.Core.Enumerations;
using FleetManagement.Core.Models;
using FleetManagement.Data;
using FleetManagement.Service;
using Microsoft.AspNetCore.Mvc;
using Xunit;

namespace FleetManagement.Tests
{
    public class JobControllerTest
    {
        private readonly MockData _mockData;

        private readonly JobController _jobController;

        public JobControllerTest()
        {
            _mockData = new MockData();
            _jobController = new JobController(new JobSheetService());
        }

        /// <summary>
        /// Test if Generate method returns with success
        /// </summary>
        [Fact]
        public void GenerateReturnsOk()
        {
            var actionResult = _jobController.GenerateJobSheet();
            Assert.IsType<OkObjectResult>(actionResult.Result);
        }

        /// <summary>
        /// Test if Generate method returns a valid job sheet object
        /// </summary>
        [Fact]
        public void GenerateReturnsJobSheet()
        {
            var okObjectResult =_jobController.GenerateJobSheet().Result as OkObjectResult;
            Assert.IsType<JobSheet>(okObjectResult.Value);
        }

        /// <summary>
        /// Test if Submit method returns with success
        /// </summary>
        [Fact]
        public void SubmitReturnsOk()
        {
            // create a job sheet
            var okObjectResult = _jobController.GenerateJobSheet().Result as OkObjectResult;
            var jobSheet = okObjectResult.Value as JobSheet;

            // test ok message
            var actionResult = _jobController.SubmitJobSheet(jobSheet);
            Assert.IsType<OkObjectResult>(actionResult.Result);
        }

        /// <summary>
        /// Test if Submit method returns a valid job decision
        /// </summary>
        [Fact]
        public void SubmitReturnsJobDecision()
        {
            // create a job sheet
            var jsOkObjectResult =_jobController.GenerateJobSheet().Result as OkObjectResult;
            var jobSheet = jsOkObjectResult.Value as JobSheet;

            // test if is a valid object
            var jdrOkObjectResult =_jobController.SubmitJobSheet(jobSheet).Result as OkObjectResult;
            Assert.IsType<JobDecisionReport>(jdrOkObjectResult.Value);
        }

        /// <summary>
        /// Test business rule: Tyres cannot exceed 4
        /// </summary>
        [Fact]
        public void SubmitReturnsTyresCannotExceedFour()
        {
            // create a job sheet
            var jsOkObjectResult =_jobController.GenerateJobSheet().Result as OkObjectResult;
            var jobSheet = jsOkObjectResult.Value as JobSheet;

            // clear work items and add 5 tyres
            jobSheet.WorkItems.Clear();
            jobSheet.WorkItems.AddRange(_mockData.GetWorkItemsByType(WorkItemType.Tyre).Take(4));
            jobSheet.WorkItems.AddRange(_mockData.GetWorkItemsByType(WorkItemType.Tyre).Take(1));

            // assert
            var jdrOkObjectResult =_jobController.SubmitJobSheet(jobSheet).Result as OkObjectResult;
            var jobDecisionReport = Assert.IsType<JobDecisionReport>(jdrOkObjectResult.Value);
            var isValid = jobDecisionReport.JobDecision == JobDecision.Declined && jobDecisionReport.Description == Messages.TyresCannotExceed4;
            Assert.True(isValid, Messages.TyresCannotExceed4);
        }

        /// <summary>
        /// Test business rule: Tyres must be changed in pairs
        /// </summary>
        [Fact]
        public void SubmitReturnsTyresMustBeChangedInPairs()
        {
            // create a job sheet
            var jsOkObjectResult =_jobController.GenerateJobSheet().Result as OkObjectResult;
            var jobSheet = jsOkObjectResult.Value as JobSheet;

            // clear work items and add 1 tyre
            jobSheet.WorkItems.Clear();
            jobSheet.WorkItems.AddRange(_mockData.GetWorkItemsByType(WorkItemType.Tyre).Take(1));

            // assert
            var jdrOkObjectResult =_jobController.SubmitJobSheet(jobSheet).Result as OkObjectResult;
            var jobDecisionReport = Assert.IsType<JobDecisionReport>(jdrOkObjectResult.Value);
            var isValid = jobDecisionReport.JobDecision == JobDecision.Declined && jobDecisionReport.Description == Messages.TyresMustBeChangedInPairs;
            Assert.True(isValid, Messages.TyresMustBeChangedInPairs);
        }

        /// <summary>
        /// Test business rule: Brake pads and discs must be changed at the same time
        /// </summary>
        [Fact]
        public void SubmitReturnsBrakePadsAndDisksMustBeChangedAtSameTime()
        {
            // create a job sheet
            var jsOkObjectResult =_jobController.GenerateJobSheet().Result as OkObjectResult;
            var jobSheet = jsOkObjectResult.Value as JobSheet;

            // clear work items and add unpaired brake items
            jobSheet.WorkItems.Clear();
            jobSheet.WorkItems.AddRange(_mockData.GetWorkItemsByType(WorkItemType.BrakePad).Take(2));
            jobSheet.WorkItems.AddRange(_mockData.GetWorkItemsByType(WorkItemType.BrakeDisc).Take(1));

            // assert
            var jdrOkObjectResult =_jobController.SubmitJobSheet(jobSheet).Result as OkObjectResult;
            var jobDecisionReport = Assert.IsType<JobDecisionReport>(jdrOkObjectResult.Value);
            var isValid = jobDecisionReport.JobDecision == JobDecision.Declined && jobDecisionReport.Description == Messages.BrakePadsAndDiscsMustBeChangedAtSameTime;
            Assert.True(isValid, Messages.BrakePadsAndDiscsMustBeChangedAtSameTime);
        }

        /// <summary>
        /// Test business rule: Can only include 1 exhaust
        /// </summary>
        [Fact]
        public void SubmitReturnsCanOnlyIncludeOneExhaust()
        {
            // create a job sheet
            var jsOkObjectResult =_jobController.GenerateJobSheet().Result as OkObjectResult;
            var jobSheet = jsOkObjectResult.Value as JobSheet;

            // clear work items and add 2 exhausts
            jobSheet.WorkItems.Clear();
            jobSheet.WorkItems.AddRange(_mockData.GetWorkItemsByType(WorkItemType.Exhaust).Take(2));

            // assert
            var jdrOkObjectResult =_jobController.SubmitJobSheet(jobSheet).Result as OkObjectResult;
            var jobDecisionReport = Assert.IsType<JobDecisionReport>(jdrOkObjectResult.Value);
            var isValid = jobDecisionReport.JobDecision == JobDecision.Declined && jobDecisionReport.Description == Messages.CanOnlyInclude1Exhaust;
            Assert.True(isValid, Messages.CanOnlyInclude1Exhaust);
        }

        /// <summary>
        /// Test business rule: The total hours labour must not exceed the reference number of hours labour
        /// </summary>
        [Fact]
        public void SubmitReturnsTotalHoursMustNotExceedTheReferenceNumberOfHours()
        {
            // create a job sheet
            var jsOkObjectResult =_jobController.GenerateJobSheet().Result as OkObjectResult;
            var jobSheet = jsOkObjectResult.Value as JobSheet;

            // reset work items and add 1 minute
            SetDefaultJobSheet(jobSheet);
            foreach (var workItem in jobSheet.WorkItems)
            {
                workItem.TimeInMinutes++;
            }

            // assert
            var jdrOkObjectResult =_jobController.SubmitJobSheet(jobSheet).Result as OkObjectResult;
            var jobDecisionReport = Assert.IsType<JobDecisionReport>(jdrOkObjectResult.Value);
            var isValid = jobDecisionReport.JobDecision == JobDecision.Declined && jobDecisionReport.Description == Messages.TotalHoursLabourMustNotExceedTheReferenceNumberOfHoursLabour;
            Assert.True(isValid, Messages.TotalHoursLabourMustNotExceedTheReferenceNumberOfHoursLabour);
        }

        /// <summary>
        /// Test overall decision: Approved - Price is within 10% of the reference price
        /// </summary>
        [Fact]
        public void SubmitReturnsApproved()
        {
            // create a job sheet
            var jsOkObjectResult =_jobController.GenerateJobSheet().Result as OkObjectResult;
            var jobSheet = jsOkObjectResult.Value as JobSheet;

            // reset work items
            SetDefaultJobSheet(jobSheet);

            // assert
            var jdrOkObjectResult =_jobController.SubmitJobSheet(jobSheet).Result as OkObjectResult;
            var jobDecisionReport = Assert.IsType<JobDecisionReport>(jdrOkObjectResult.Value);
            var isValid = jobDecisionReport.JobDecision == JobDecision.Approved && jobDecisionReport.Description == Messages.PriceIsWithin10PercentOfTheReferencePrice;
            Assert.True(isValid, Messages.PriceIsWithin10PercentOfTheReferencePrice);
        }

        /// <summary>
        /// Test overall decision: Refered - Total price is between 10% and 15% of the reference price
        /// </summary>
        [Fact]
        public void SubmitReturnsRefered()
        {
            // create a job sheet
            var jsOkObjectResult =_jobController.GenerateJobSheet().Result as OkObjectResult;
            var jobSheet = jsOkObjectResult.Value as JobSheet;

            // reset work items and increment the cost with 11%
            SetDefaultJobSheet(jobSheet, 1.11m);

            // assert
            var jdrOkObjectResult =_jobController.SubmitJobSheet(jobSheet).Result as OkObjectResult;
            var jobDecisionReport = Assert.IsType<JobDecisionReport>(jdrOkObjectResult.Value);
            var isValid = jobDecisionReport.JobDecision == JobDecision.Refered && jobDecisionReport.Description == Messages.TotalPriceIsBetween10and15PercentOfTheReferencePrice;
            Assert.True(isValid, Messages.TotalPriceIsBetween10and15PercentOfTheReferencePrice);
        }

        /// <summary>
        /// Test overall decision: Declined - Price exceeds 15% of the reference price
        /// </summary>
        [Fact]
        public void SubmitReturnsDeclined()
        {
            // create a job sheet
            var jsOkObjectResult =_jobController.GenerateJobSheet().Result as OkObjectResult;
            var jobSheet = jsOkObjectResult.Value as JobSheet;

            // reset work items and increment the cost with 11%
            SetDefaultJobSheet(jobSheet, 1.16m);

            // assert
            var jdrOkObjectResult =_jobController.SubmitJobSheet(jobSheet).Result as OkObjectResult;
            var jobDecisionReport = Assert.IsType<JobDecisionReport>(jdrOkObjectResult.Value);
            var isValid = jobDecisionReport.JobDecision == JobDecision.Declined && jobDecisionReport.Description == Messages.PriceExceeds15PercentOfTheReferencePrice;
            Assert.True(isValid, Messages.PriceExceeds15PercentOfTheReferencePrice);
        }

        private void SetDefaultJobSheet(JobSheet jobSheet, decimal increment = 1m)
        {
            var mockData = new MockData();

            // update reference data
            var referenceData = mockData.ReferenceData;
            foreach (var item in referenceData)
            {
                var labourCost = item.TimeInMinutes * mockData.LabourCostPerHour / 60m;
                var referenceLabourCost = item.TimeInMinutes * increment * mockData.LabourCostPerHour / 60m;
                var costDiff = referenceLabourCost - labourCost;
                item.Cost = item.Cost * increment + costDiff;
            }

            // reset work items
            jobSheet.WorkItems.Clear();
            jobSheet.WorkItems.Add(referenceData.FirstOrDefault(w => w.Item == WorkItemType.Tyre));
            jobSheet.WorkItems.Add(referenceData.FirstOrDefault(w => w.Item == WorkItemType.Tyre));
            jobSheet.WorkItems.Add(referenceData.FirstOrDefault(w => w.Item == WorkItemType.BrakePad));
            jobSheet.WorkItems.Add(referenceData.FirstOrDefault(w => w.Item == WorkItemType.BrakeDisc));
            jobSheet.WorkItems.Add(referenceData.FirstOrDefault(w => w.Item == WorkItemType.Oil));
            jobSheet.WorkItems.Add(referenceData.FirstOrDefault(w => w.Item == WorkItemType.Exhaust));

            // update total
            jobSheet.TotalCost = jobSheet.GetLabourTotalCost();
        }
    }
}
