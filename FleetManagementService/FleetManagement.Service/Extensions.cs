﻿using System.Collections.Generic;
using System.Linq;
using FleetManagement.Core.Enumerations;
using FleetManagement.Core.Models;
using FleetManagement.Data;

namespace FleetManagement.Service
{
    public static class Extensions
    {
        /// <summary>
        /// Gets the total cost.
        /// </summary>
        /// <returns>The total cost.</returns>
        /// <param name="jobSheet">Job sheet.</param>
        public static decimal GetLabourTotalCost(this JobSheet jobSheet)
        {
            var mockData = new MockData();

            decimal totalCost = 0;

            foreach (var item in jobSheet.WorkItems)
            {
                decimal labourCost = item.TimeInMinutes * mockData.LabourCostPerHour / 60;
                totalCost += item.Cost + labourCost;
            }

            return totalCost;
        }

        /// <summary>
        /// Check whether the time is valid.
        /// The total hours labour must not exceed the reference number of hours labour.
        /// </summary>
        /// <returns><c>true</c>, if labour time valid was valid, <c>false</c> otherwise.</returns>
        /// <param name="jobSheet">Job sheet.</param>
        public static bool IsLabourTimeValid(this JobSheet jobSheet)
        {
            var mockData = new MockData();

            // get total cost
            var totalMin = 0;
            var totalMinReference = 0;

            foreach (var item in jobSheet.WorkItems)
            {
                totalMin += item.TimeInMinutes;
                totalMinReference += mockData.ReferenceData.FirstOrDefault(r => r.Item == item.Item).TimeInMinutes;
            }

            return totalMin > 0 && totalMin <= totalMinReference;
        }
    }
}
