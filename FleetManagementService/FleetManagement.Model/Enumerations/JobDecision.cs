﻿namespace FleetManagement.Core.Enumerations
{
    public enum JobDecision
    {
        Unknown,
        Approved,
        Refered,
        Declined
    }
}