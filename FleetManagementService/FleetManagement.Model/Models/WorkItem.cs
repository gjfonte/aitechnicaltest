﻿using FleetManagement.Core.Enumerations;

namespace FleetManagement.Core.Models
{
    public class WorkItem
    {
        public WorkItem()
        {
            Item = WorkItemType.Unknown;
        }

        public int WorkItemID { get; set; }
        public WorkItemType Item { get; set; }
        public int TimeInMinutes { get; set; }
        public decimal Cost { get; set; }
    }
}