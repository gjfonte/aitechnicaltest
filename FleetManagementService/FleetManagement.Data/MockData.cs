﻿using System.Collections.Generic;
using System.Linq;
using FleetManagement.Core.Enumerations;
using FleetManagement.Core.Models;

namespace FleetManagement.Data
{
    /// <summary>
    /// Class responsible to maintain the mock data
    /// </summary>
    public class MockData
    {
        private readonly List<Vehicle> _vehicles = new List<Vehicle>();
        private readonly List<WorkItem> _workItems = new List<WorkItem>();
        private readonly List<WorkItem> _referenceData = new List<WorkItem>();

        public MockData()
        {
            LabourCostPerHour = 45;
            InitData();
        }

        public decimal LabourCostPerHour { get; }

        public List<Vehicle> Vehicles => _vehicles;

        public List<WorkItem> WorkItems => _workItems;

        public List<WorkItem> ReferenceData => _referenceData;

        private void InitData()
        {
            _vehicles.AddRange(new List<Vehicle> 
            {
                new Vehicle
                {
                    VehicleID = 1,
                    Year = 1994,
                    Make = "Acura",
                    Model = "Legend",
                    RegistrationPlate = "1G6DP1"
                },
                new Vehicle
                {
                    VehicleID = 2,
                    Year = 1985,
                    Make = "Plymouth",
                    Model = "Voyager",
                    RegistrationPlate = "3N1CE2"
                },
                new Vehicle
                {
                    VehicleID = 3,
                    Year = 1991,
                    Make = "Mitsubishi",
                    Model = "GTO",
                    RegistrationPlate = "WAULD5"
                },
                new Vehicle
                {
                    VehicleID = 4,
                    Year = 2008,
                    Make = "Honda",
                    Model = "Pilot",
                    RegistrationPlate = "1GYS3D"
                },
                new Vehicle
                {
                    VehicleID = 5,
                    Year = 2011,
                    Make = "Volkswagen",
                    Model = "Tiguan",
                    RegistrationPlate = "SAJWA4"
                },
                new Vehicle
                {
                    VehicleID = 6,
                    Year = 1966,
                    Make = "Jensen",
                    Model = "Interceptor",
                    RegistrationPlate = "5N1AA0"
                },
                new Vehicle
                {
                    VehicleID = 7,
                    Year = 2012,
                    Make = "Lincoln",
                    Model = "Navigator L",
                    RegistrationPlate = "1D4RE4"
                },
                new Vehicle
                {
                    VehicleID = 8,
                    Year = 2006,
                    Make = "Ford",
                    Model = "E250",
                    RegistrationPlate = "WA1LYB"
                },
                new Vehicle
                {
                    VehicleID = 9,
                    Year = 2004,
                    Make = "Toyota",
                    Model = "Highlander",
                    RegistrationPlate = "WUAW2B"
                },
                new Vehicle
                {
                    VehicleID = 10,
                    Year = 1989,
                    Make = "Audi",
                    Model = "200",
                    RegistrationPlate = "WAUSH7"
                }
            });

            _workItems.AddRange(new List<WorkItem>
            {
                new WorkItem
                {
                    WorkItemID = 1,
                    Item = WorkItemType.Tyre,
                    TimeInMinutes = 30,
                    Cost = 200
                },
                new WorkItem
                {
                    WorkItemID = 2,
                    Item = WorkItemType.Tyre,
                    TimeInMinutes = 28,
                    Cost = 220
                },
                new WorkItem
                {
                    WorkItemID = 3,
                    Item = WorkItemType.Tyre,
                    TimeInMinutes = 27,
                    Cost = 230
                },
                new WorkItem
                {
                    WorkItemID = 4,
                    Item = WorkItemType.Tyre,
                    TimeInMinutes = 30,
                    Cost = 230
                },
                new WorkItem
                {
                    WorkItemID = 5,
                    Item = WorkItemType.BrakeDisc,
                    TimeInMinutes = 90,
                    Cost = 100
                },
                new WorkItem
                {
                    WorkItemID = 6,
                    Item = WorkItemType.BrakeDisc,
                    TimeInMinutes = 85,
                    Cost = 110
                },
                new WorkItem
                {
                    WorkItemID = 7,
                    Item = WorkItemType.BrakeDisc,
                    TimeInMinutes = 80,
                    Cost = 115
                },
                new WorkItem
                {
                    WorkItemID = 8,
                    Item = WorkItemType.BrakeDisc,
                    TimeInMinutes = 90,
                    Cost = 115
                },
                new WorkItem
                {
                    WorkItemID = 9,
                    Item = WorkItemType.BrakePad,
                    TimeInMinutes = 60,
                    Cost = 50
                },
                new WorkItem
                {
                    WorkItemID = 10,
                    Item = WorkItemType.BrakePad,
                    TimeInMinutes = 56,
                    Cost = 55
                },
                new WorkItem
                {
                    WorkItemID = 11,
                    Item = WorkItemType.BrakePad,
                    TimeInMinutes = 54,
                    Cost = 57
                },
                new WorkItem
                {
                    WorkItemID = 12,
                    Item = WorkItemType.BrakePad,
                    TimeInMinutes = 60,
                    Cost = 57
                },
                new WorkItem
                {
                    WorkItemID = 13,
                    Item = WorkItemType.Oil,
                    TimeInMinutes = 30,
                    Cost = 20
                },
                new WorkItem
                {
                    WorkItemID = 14,
                    Item = WorkItemType.Oil,
                    TimeInMinutes = 28,
                    Cost = 22
                },
                new WorkItem
                {
                    WorkItemID = 15,
                    Item = WorkItemType.Oil,
                    TimeInMinutes = 27,
                    Cost = 23
                },
                new WorkItem
                {
                    WorkItemID = 16,
                    Item = WorkItemType.Oil,
                    TimeInMinutes = 30,
                    Cost = 23
                },
                new WorkItem
                {
                    WorkItemID = 17,
                    Item = WorkItemType.Exhaust,
                    TimeInMinutes = 240,
                    Cost = 175
                },
                new WorkItem
                {
                    WorkItemID = 18,
                    Item = WorkItemType.Exhaust,
                    TimeInMinutes = 230,
                    Cost = 302
                },
                new WorkItem
                {
                    WorkItemID = 19,
                    Item = WorkItemType.Exhaust,
                    TimeInMinutes = 220,
                    Cost = 316
                },
                new WorkItem
                {
                    WorkItemID = 20,
                    Item = WorkItemType.Exhaust,
                    TimeInMinutes = 240,
                    Cost = 316
                }
            });

            _referenceData.AddRange(new List<WorkItem>
            {
                new WorkItem
                {
                    WorkItemID = 1,
                    Item = WorkItemType.Tyre,
                    TimeInMinutes = 30,
                    Cost = 200
                },
                new WorkItem
                {
                    WorkItemID = 2,
                    Item = WorkItemType.BrakeDisc,
                    TimeInMinutes = 90,
                    Cost = 100
                },
                new WorkItem
                {
                    WorkItemID = 3,
                    Item = WorkItemType.BrakePad,
                    TimeInMinutes = 60,
                    Cost = 50
                },
                new WorkItem
                {
                    WorkItemID = 4,
                    Item = WorkItemType.Oil,
                    TimeInMinutes = 30,
                    Cost = 20
                },
                new WorkItem
                {
                    WorkItemID = 5,
                    Item = WorkItemType.Exhaust,
                    TimeInMinutes = 240,
                    Cost = 175
                }
            });
        }

        public List<WorkItem> GetWorkItemsByType (WorkItemType workItemType)
        {
            return WorkItems.Where(w => w.Item == workItemType).ToList();
        }
    }
}
