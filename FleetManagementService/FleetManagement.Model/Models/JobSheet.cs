﻿using System;
using System.Collections.Generic;
using FleetManagement.Core.Enumerations;

namespace FleetManagement.Core.Models
{
    public class JobSheet
    {
        public JobSheet()
        {
            JobSheeID = -1;
            WorkItems = new List<WorkItem>();
            JobDecisionDetails = new JobDecisionReport();
        }

        public int JobSheeID { get; set; }
        public DateTime JobDate { get; set; }
        public Vehicle Vehicle { get; set; }
        public List<WorkItem> WorkItems { get; set; }
        public decimal TotalCost { get; set; }
        public bool Submitted { get; set; }
        public JobDecisionReport JobDecisionDetails { get; set; }
    }
}
