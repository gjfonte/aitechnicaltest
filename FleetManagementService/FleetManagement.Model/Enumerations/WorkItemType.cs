﻿namespace FleetManagement.Core.Enumerations
{
    public enum WorkItemType
    {
        Unknown,
        Tyre,
        BrakeDisc,
        BrakePad,
        Oil,
        Exhaust
    }
}