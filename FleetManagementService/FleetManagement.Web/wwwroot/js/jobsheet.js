var ai = ai || {};

ai.job = function(){
    var module = this;

    // vars
    var apiUrl = "http://localhost:5001";
    var currentJob;

    // public functions
    module.getJob = function(){

        $.ajax({
            type: 'GET',
            url: apiUrl + "/api/job/generate",
            success: function (data) {
                currentJob = data;
                showJobDetails(currentJob);
            },
            error: function(error){
                alert("Error generating a new job sheet. Please check the API is available...")
                console.log(error);
            }
        });
    }

    module.submitJob = function(){
        $.ajax({
            type: 'POST',
            accepts: 'application/json',
            url: apiUrl + "/api/job/submit",
            contentType: 'application/json',
            data: JSON.stringify(currentJob),
            success: function (result) {
                currentJob.submitted = true;
                currentJob.jobDecisionDetails = result;
                saveJobToMemory();
            },
            error: function (error) {
                alert("Error submitting the job sheet. Please check the API is available...")
                console.log(error);
            }
        });
    }

    // private functions
    function showJobDetails(){
        var btnGetJob = document.getElementById("btnGetJob");
        btnGetJob.style.display = "none";

        var wrapper = document.getElementById("wrapJobDetails");
        wrapper.style.display = "block";

        document.getElementById("lblCarMake").innerHTML = currentJob.vehicle.make;
        document.getElementById("lblCarModel").innerHTML = currentJob.vehicle.model;
        document.getElementById("lblTotalItems").innerHTML = currentJob.workItems.length;
        document.getElementById("lblTotalCost").innerHTML = currentJob.totalCost.toFixed(2);
    }

    function saveJobToMemory(){
        $.ajax({
            type: 'POST',
            url: "/Home/SaveJob",
            dataType: 'json',
            contentType: 'application/json',
            data: JSON.stringify(currentJob),
            success: function (result) {
                if (result.success && result.success == true){
                    window.location.replace("/home/jobresult?id=" + result.jobId);
                }
            },
            error: function (error) {
                alert("Error saving the job sheet...")
                console.log(error);
            }
        });
    }

    function init(){
        console.log("Job Sheet manager is running")
    }

    // init module
    init();

    return module;
}

var jobService = new ai.job();