﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using FleetManagement.Web.Models;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Configuration;
using FleetManagement.Core.Models;

namespace FleetManagement.Web.Controllers
{
    public class HomeController : BaseController
    {
        private const string cacheKeyJobs = "ListOfJobs";
        private readonly List<JobSheet> _listOfJobs;
        private readonly string _apiServiceUrl;

        public HomeController(IMemoryCache cache, IConfiguration configuration) : base(cache)
        {
            _listOfJobs = GetFromCache();
            _apiServiceUrl = configuration.GetValue<string>("ApiServiceUrl");
        }

        public IActionResult Index()
        {
            ViewData["apiUrl"] = _apiServiceUrl;
            return View();
        }

        [HttpPost]
        public IActionResult SaveJob([FromBody] JobSheet jobSheet)
        {
            var jobID = AddJob(jobSheet);

            return new JsonResult(new { success = true, jobId = jobID });
        }

        public IActionResult JobResult(int id)
        {
            var job = GetJob(id);

            return View(job);
        }

        public IActionResult AllJobs()
        {
            return View(_listOfJobs);
        }

        public IActionResult About()
        {
            return View();
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }

        #region Private functions

        private List<JobSheet> GetFromCache()
        {
            // all jobs will be store in the memory cache
            var listOfJobs = _cache.Get<List<JobSheet>>(cacheKeyJobs);
            if (listOfJobs == null)
            {
                listOfJobs = new List<JobSheet>();
                _cache.Set(cacheKeyJobs, listOfJobs);
            }

            return listOfJobs;
        }

        private int AddJob(JobSheet jobSheet)
        {
            jobSheet.JobSheeID = _listOfJobs.Count + 1;
            _listOfJobs.Add(jobSheet);

            return jobSheet.JobSheeID;
        }

        private JobSheet GetJob(int jobID)
        {
            var job = _listOfJobs.FirstOrDefault(j => j.JobSheeID == jobID);
            if (job != null)
            {
                return job;
            }

            throw new KeyNotFoundException($"JobID [{jobID}] not found");
        }

        #endregion
    }
}
