﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FleetManagement.Core
{
    public static class Messages
    {
        public static string TyresCannotExceed4 = "Tyres cannot exceed 4";

        public static string TyresMustBeChangedInPairs = "Tyres must be changed in pairs";

        public static string BrakePadsAndDiscsMustBeChangedAtSameTime = "Brake pads and discs must be changed at the same time";

        public static string CanOnlyInclude1Exhaust = "Can only include 1 exhaust";

        public static string TotalHoursLabourMustNotExceedTheReferenceNumberOfHoursLabour = "The total hours labour must not exceed the reference number of hours labour";

        public static string PriceIsWithin10PercentOfTheReferencePrice = "Price is within 10% of the reference price";

        public static string TotalPriceIsBetween10and15PercentOfTheReferencePrice = "Total price is between 10% and 15% of the reference price";

        public static string PriceExceeds15PercentOfTheReferencePrice = "Price exceeds 15% of the reference price";

    }
}
